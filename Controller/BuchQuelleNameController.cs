﻿using LiteraturQuelleModule.Controller;
using LiteraturQuelleModule.Models;
using LiteraturQuelleModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using clsOntologyItem = OntologyClasses.BaseClasses.clsOntologyItem;

namespace LiteraturQuelleModule.Controller
{
    public class BuchQuelleNameController : QuelleNameBaseController, INameTransform
    {
        public override string IdClassResponsible
        {
            get
            {
                return Config.LocalData.Class_Buch_Quellenangabe.GUID;
            }
        }

        public bool IsReferenceCompatible => true;

        public bool IsAspNetProject { get; set; }

        public bool IsResponsible(string idClass)
        {
            return idClass == IdClassResponsible;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = items
               };

               var idClasses = new System.Collections.Generic.List<string>();
               if (string.IsNullOrEmpty(idClass))
               {
                   idClasses = items.GroupBy(itm => itm.GUID_Parent).Select(grp => grp.Key).ToList();
               }
               else
               {
                   idClasses.Add(idClass);
               }

               var idClassesNotSupported = idClasses.Where(cls => cls != IdClassResponsible).ToList();
               if (idClassesNotSupported.Count > 0)
               {
                   messageOutput?.OutputError($"There are {idClassesNotSupported.Count} invalid classes!");
                   return result;
               }
               messageOutput?.OutputInfo("Checked Class-Ids.");

               var elasticAgent = new ElasticAgent(Globals);
               var request = new SetQuelleStringOfQuelleRequest(SetNameOfQuelle.Config.LocalData.Object_Default.GUID)
               {
                   MessageOutpupt = messageOutput
               };
               var configResult = await elasticAgent.GetSetQuelleStringOfQuelleConfig(request);
               result.ResultState = configResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var getQuellNamesResult = await GetQuellNames(items, configResult.Result, encodeName);

               result.ResultState = getQuellNamesResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetQuellNames(List<clsOntologyItem> quellen, SetQuelleStringOfQuelleConfig config, bool encodeName)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = quellen
                };

                var searchCreatedQuellen = quellen.Select(quell => new clsObjectAtt
                {
                    ID_Object = quell.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Quelle.GUID
                }).ToList();

                var dbReaderCreatedQuelle = new OntologyModDBConnector(Globals);
                if (searchCreatedQuellen.Any())
                {
                    result.ResultState = dbReaderCreatedQuelle.GetDataObjectAtt(searchCreatedQuellen);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Author!";
                        return result;
                    }
                }

                var quellenToCreate = (from quelle in quellen
                           join createdQuelle in dbReaderCreatedQuelle.ObjAtts on quelle.GUID equals createdQuelle.ID_Object into createdQuelleList
                           from createdQuelle in createdQuelleList.DefaultIfEmpty()
                           where createdQuelle == null
                           select quelle).ToList();

                if (quellenToCreate.Any())
                {
                    //ToDo: Implement
                    quellenToCreate.ForEach(quell =>
                    {
                        if (encodeName)
                        {
                            quell.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(quell.Name));
                        }
                    });
                }
                
                if (dbReaderCreatedQuelle.ObjAtts.Any())
                {
                    (from quelle in quellen
                     join createdQuelle in dbReaderCreatedQuelle.ObjAtts on quelle.GUID equals createdQuelle.ID_Object
                     select new { quelle, createdQuelle }).ToList().ForEach(quelle =>
                       {
                           if (encodeName)
                           {
                               quelle.quelle.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(quelle.createdQuelle.Val_String));
                           }
                           else
                           {
                               quelle.quelle.Name = quelle.createdQuelle.Val_String;
                           }
                       });
                }
                
                return result;
            });

            return taskResult;
        }

        public BuchQuelleNameController(Globals globals) : base(globals)
        {

        }
    }
}
