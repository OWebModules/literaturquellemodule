﻿using LiteraturQuelleModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Controller
{
    public abstract class QuelleNameBaseController
    {
        public Globals Globals { get; private set; }
        public abstract string IdClassResponsible { get; }

        public virtual async Task<clsOntologyItem> SetName(SetQuelleStringOfQuelleConfig config)
        {
            throw new NotImplementedException();
        }

        public QuelleNameBaseController(Globals globals)
        {
            Globals = globals;
        }
    }
}
