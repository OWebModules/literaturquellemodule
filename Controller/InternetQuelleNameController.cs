﻿using LiteraturQuelleModule.Controller;
using LiteraturQuelleModule.Models;
using LiteraturQuelleModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using clsOntologyItem = OntologyClasses.BaseClasses.clsOntologyItem;

namespace LiteraturQuelleModule.Controller
{
    public class InternetQuelleNameController : QuelleNameBaseController, INameTransform
    {
        public override string IdClassResponsible
        {
            get
            {
                return Config.LocalData.Class_Internet_Quellenangabe.GUID;
            }
        }

        public bool IsReferenceCompatible => true;

        public bool IsAspNetProject { get; set; }

        public override async Task<clsOntologyItem> SetName(SetQuelleStringOfQuelleConfig config)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();

                var quellen = config.QuellenToLiteraturQuellen.Where(quell => quell.ID_Parent_Object == IdClassResponsible).ToList();

                var searchPartner = quellen.Select(quell => new clsObjectRel
                {
                    ID_Object = quell.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_Class_Right
                }).ToList();

                var dbReaderPartner = new OntologyModDBConnector(Globals);

                if (searchPartner.Any())
                {
                    result = dbReaderPartner.GetDataObjectRel(searchPartner);

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while getting the Author!";
                        return result;
                    }
                }

                var searchYear = quellen.Select(quell =>
                    new clsObjectAtt
                    {
                        ID_Object = quell.ID_Object,
                        ID_AttributeType = Config.LocalData.AttributeType_Jahr.GUID
                    }).ToList();

                var dbReaderYear = new OntologyModDBConnector(Globals);

                if (searchYear.Any())
                {
                    result = dbReaderYear.GetDataObjectAtt(searchYear);

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while getting the publishing-year!";
                        return result;
                    }
                }
                
                var searchLogEntry = quellen.Select(quell => new clsObjectRel
                {
                    ID_Object = quell.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_download_Logentry.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_download_Logentry.ID_Class_Right
                }).ToList();

                var dbReaderLogEntry = new OntologyModDBConnector(Globals);
                var dbReaderDownload = new OntologyModDBConnector(Globals);

                if (searchLogEntry.Any())
                {
                    result = dbReaderLogEntry.GetDataObjectRel(searchLogEntry);

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while getting the Logentry!";
                        return result;
                    }

                    var searchDownloadStamp = dbReaderLogEntry.ObjectRels.Select(rel => new clsObjectAtt
                    {
                        ID_Object = rel.ID_Other,
                        ID_AttributeType = Config.LocalData.AttributeType_DateTimestamp.GUID
                    }).ToList();

                    if (searchDownloadStamp.Any())
                    {
                        result = dbReaderDownload.GetDataObjectAtt(searchDownloadStamp);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            result.Additional1 = "Error while getting the Download-Stamp!";
                            return result;
                        }
                    }
                }

                var dbReaderUrl = new OntologyModDBConnector(Globals);

                var searchUrl = quellen.Select(quell => new clsObjectRel
                {
                    ID_Object = quell.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_Class_Right
                }).ToList();

                if (searchUrl.Any())
                {
                    result = dbReaderUrl.GetDataObjectRel(searchUrl);

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while getting the Urls!";
                        return result;
                    }
                }

                var logEntries = (from logEntry in dbReaderLogEntry.ObjectRels
                                  join downloadStamp in dbReaderDownload.ObjAtts on logEntry.ID_Other equals downloadStamp.ID_Object
                                  select new { logEntry, downloadStamp }).ToList();
                var template = config.TemplateInternetQuelle.Val_String;

                var regexYear = new Regex(@"@YEAR@(\|\S+)?");

                var yearVariable = "@YEAR@";
                var yearReplace = "";
                var yearStringMatch = regexYear.Match(template);
                if (yearStringMatch.Success)
                {
                    yearVariable = yearStringMatch.Value;
                    yearReplace = yearVariable.Replace("@YEAR@|", "");
                }

                var relationConfig = new clsRelationConfig(Globals);
                var saveAttributes = new List<clsObjectAtt>();

                foreach (var quelle in (from quellToLitQuell in quellen
                                      join year in dbReaderYear.ObjAtts on quellToLitQuell.ID_Object equals year.ID_Object into years
                                      from year in years.DefaultIfEmpty()
                                      join logEntry in logEntries on quellToLitQuell.ID_Object equals logEntry.logEntry.ID_Object into logEntries1
                                      from logEntry in logEntries1.DefaultIfEmpty()
                                      join url in dbReaderUrl.ObjectRels on quellToLitQuell.ID_Object equals url.ID_Object into urls
                                      from url in urls.DefaultIfEmpty()
                                      join quellString in config.QuelleToQuellString on quellToLitQuell.ID_Object equals quellString.ID_Object into quellStrings
                                      from quellString in quellStrings.DefaultIfEmpty()
                                      select new { quellToLitQuell, year, logEntry, url, quellString }))
                {
                    var quellAngabe = template;

                    var partnerList = string.Join(", ", dbReaderPartner.ObjectRels.Where(rel => rel.ID_Object == quelle.quellToLitQuell.ID_Object).Select(rel => rel.Name_Other));

                    if (!string.IsNullOrEmpty(partnerList))
                    {
                        quellAngabe = quellAngabe.Replace("@PARTNER@", partnerList);
                    }
                    else
                    {
                        quellAngabe = quellAngabe.Replace("@PARTNER@", "-");
                    }

                    quellAngabe = quellAngabe.Replace("@TITLE@", quelle.quellToLitQuell.Name_Object);

                    if (quelle.year != null)
                    {
                        quellAngabe = quellAngabe.Replace(yearVariable, quelle.year.Val_Lng.ToString());
                    }
                    else
                    {
                        quellAngabe = quellAngabe.Replace(yearVariable, yearReplace);
                    }

                    if (quelle.url != null)
                    {
                        quellAngabe = quellAngabe.Replace("@URL@", quelle.url.Name_Other);
                    }
                    else
                    {
                        quellAngabe = quellAngabe.Replace("@URL@", "-");
                    }

                    if (quelle.logEntry != null)
                    {
                        quellAngabe = quellAngabe.Replace("@DOWNLOAD_STAMP@", quelle.logEntry.downloadStamp.Val_Date.Value.ToString());
                    }
                    else
                    {
                        quellAngabe = quellAngabe.Replace("@DOWNLOAD_STAMP@", "-");
                    }

                    var saveQuellString = true;
                    clsObjectAtt quellStringAtt = null;
                    if (quelle.quellString != null)
                    {
                        quellStringAtt = quelle.quellString;
                        if (quelle.quellString.Val_String == quellAngabe)
                        {
                            saveQuellString = false;
                        }
                        else
                        {
                            quellStringAtt.Val_String = quellAngabe;
                            quellStringAtt.Val_Name = quellAngabe.Length > 255 ? quellAngabe.Substring(0, 255) : quellAngabe;
                        }
                    }
                    else
                    {
                        quellStringAtt = relationConfig.Rel_ObjectAttribute(new clsOntologyItem
                        {
                            GUID = quelle.quellToLitQuell.ID_Object,
                            Name = quelle.quellToLitQuell.Name_Object,
                            GUID_Parent = quelle.quellToLitQuell.ID_Parent_Object,
                            Type = Globals.Type_Object
                        }, Config.LocalData.AttributeType_Quelle, quellAngabe);
                    }

                    if (saveQuellString)
                    {
                        saveAttributes.Add(quellStringAtt);
                    }
                } 


                if (saveAttributes.Any())
                {
                    var dbWriter = new OntologyModDBConnector(Globals);

                    result = dbWriter.SaveObjAtt(saveAttributes);

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Quellstrings!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public bool IsResponsible(string idClass)
        {
            return idClass == IdClassResponsible;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = items
               };

               var idClasses = new System.Collections.Generic.List<string>();
               if (string.IsNullOrEmpty(idClass))
               {
                   idClasses = items.GroupBy(itm => itm.GUID_Parent).Select(grp => grp.Key).ToList();
               }
               else
               {
                   idClasses.Add(idClass);
               }

               var idClassesNotSupported = idClasses.Where(cls => cls != IdClassResponsible).ToList();
               if (idClassesNotSupported.Count > 0)
               {
                   messageOutput?.OutputError($"There are {idClassesNotSupported.Count} invalid classes!");
                   return result;
               }
               messageOutput?.OutputInfo("Checked Class-Ids.");

               var elasticAgent = new ElasticAgent(Globals);
               var request = new SetQuelleStringOfQuelleRequest(SetNameOfQuelle.Config.LocalData.Object_Default.GUID)
               {
                   MessageOutpupt = messageOutput
               };
               var configResult = await elasticAgent.GetSetQuelleStringOfQuelleConfig(request);
               result.ResultState = configResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var getQuellNamesResult = await GetQuellNames(items, configResult.Result, encodeName);

               result.ResultState = getQuellNamesResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetQuellNames(List<clsOntologyItem> quellen, SetQuelleStringOfQuelleConfig config, bool encodeName)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = quellen
                };

                var searchCreatedQuellen = quellen.Select(quell => new clsObjectAtt
                {
                    ID_Object = quell.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Quelle.GUID
                }).ToList();

                var dbReaderCreatedQuelle = new OntologyModDBConnector(Globals);
                if (searchCreatedQuellen.Any())
                {
                    result.ResultState = dbReaderCreatedQuelle.GetDataObjectAtt(searchCreatedQuellen);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Author!";
                        return result;
                    }
                }

                var quellenToCreate = (from quelle in quellen
                           join createdQuelle in dbReaderCreatedQuelle.ObjAtts on quelle.GUID equals createdQuelle.ID_Object into createdQuelleList
                           from createdQuelle in createdQuelleList.DefaultIfEmpty()
                           where createdQuelle == null
                           select quelle).ToList();

                if (quellenToCreate.Any())
                {
                    var searchPartner = quellenToCreate.Select(quell => new clsObjectRel
                    {
                        ID_Object = quell.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_Class_Right
                    }).ToList();

                    var dbReaderPartner = new OntologyModDBConnector(Globals);

                    if (searchPartner.Any())
                    {
                        result.ResultState = dbReaderPartner.GetDataObjectRel(searchPartner);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the Author!";
                            return result;
                        }
                    }

                    var searchYear = quellenToCreate.Select(quell =>
                        new clsObjectAtt
                        {
                            ID_Object = quell.GUID,
                            ID_AttributeType = Config.LocalData.AttributeType_Jahr.GUID
                        }).ToList();

                    var dbReaderYear = new OntologyModDBConnector(Globals);

                    if (searchYear.Any())
                    {
                        result.ResultState = dbReaderYear.GetDataObjectAtt(searchYear);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the publishing-year!";
                            return result;
                        }
                    }

                    var searchLogEntry = quellenToCreate.Select(quell => new clsObjectRel
                    {
                        ID_Object = quell.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_download_Logentry.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_download_Logentry.ID_Class_Right
                    }).ToList();

                    var dbReaderLogEntry = new OntologyModDBConnector(Globals);
                    var dbReaderDownload = new OntologyModDBConnector(Globals);

                    if (searchLogEntry.Any())
                    {
                        result.ResultState = dbReaderLogEntry.GetDataObjectRel(searchLogEntry);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the Logentry!";
                            return result;
                        }

                        var searchDownloadStamp = dbReaderLogEntry.ObjectRels.Select(rel => new clsObjectAtt
                        {
                            ID_Object = rel.ID_Other,
                            ID_AttributeType = Config.LocalData.AttributeType_DateTimestamp.GUID
                        }).ToList();

                        if (searchDownloadStamp.Any())
                        {
                            result.ResultState = dbReaderDownload.GetDataObjectAtt(searchDownloadStamp);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while getting the Download-Stamp!";
                                return result;
                            }
                        }
                    }

                    var dbReaderUrl = new OntologyModDBConnector(Globals);

                    var searchUrl = quellenToCreate.Select(quell => new clsObjectRel
                    {
                        ID_Object = quell.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_Class_Right
                    }).ToList();

                    if (searchUrl.Any())
                    {
                        result.ResultState = dbReaderUrl.GetDataObjectRel(searchUrl);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the Urls!";
                            return result;
                        }
                    }

                    var logEntries = (from logEntry in dbReaderLogEntry.ObjectRels
                                      join downloadStamp in dbReaderDownload.ObjAtts on logEntry.ID_Other equals downloadStamp.ID_Object
                                      select new { logEntry, downloadStamp }).ToList();
                    var template = "@PARTNER@:@TITLE@;@URL@;Download @DOWNLOAD_STAMP@";
                    if (config?.TemplateInternetQuelle != null)
                    {
                        template = config.TemplateInternetQuelle.Val_String;
                    }

                    var regexYear = new Regex(@"@YEAR@(\|\S+)?");

                    var yearVariable = "@YEAR@";
                    var yearReplace = "";
                    var yearStringMatch = regexYear.Match(template);
                    if (yearStringMatch.Success)
                    {
                        yearVariable = yearStringMatch.Value;
                        yearReplace = yearVariable.Replace("@YEAR@|", "");
                    }

                    var relationConfig = new clsRelationConfig(Globals);
                    var saveAttributes = new List<clsObjectAtt>();

                    foreach (var quelle in (from quell in quellenToCreate
                                            join year in dbReaderYear.ObjAtts on quell.GUID equals year.ID_Object into years
                                            from year in years.DefaultIfEmpty()
                                            join logEntry in logEntries on quell.GUID equals logEntry.logEntry.ID_Object into logEntries1
                                            from logEntry in logEntries1.DefaultIfEmpty()
                                            join url in dbReaderUrl.ObjectRels on quell.GUID equals url.ID_Object into urls
                                            from url in urls.DefaultIfEmpty()
                                            join quellString in config.QuelleToQuellString on quell.GUID equals quellString.ID_Object into quellStrings
                                            from quellString in quellStrings.DefaultIfEmpty()
                                            select new { quell, year, logEntry, url, quellString }))
                    {
                        var quellAngabe = template;

                        var partnerList = string.Join(", ", dbReaderPartner.ObjectRels.Where(rel => rel.ID_Object == quelle.quell.GUID).Select(rel => rel.Name_Other));

                        if (!string.IsNullOrEmpty(partnerList))
                        {
                            quellAngabe = quellAngabe.Replace("@PARTNER@", partnerList);
                        }
                        else
                        {
                            quellAngabe = quellAngabe.Replace("@PARTNER@", "-");
                        }

                        quellAngabe = quellAngabe.Replace("@TITLE@", quelle.quell.Name);

                        if (quelle.year != null)
                        {
                            quellAngabe = quellAngabe.Replace(yearVariable, quelle.year.Val_Lng.ToString());
                        }
                        else
                        {
                            quellAngabe = quellAngabe.Replace(yearVariable, yearReplace);
                        }

                        if (quelle.url != null)
                        {
                            quellAngabe = quellAngabe.Replace("@URL@", quelle.url.Name_Other);
                        }
                        else
                        {
                            quellAngabe = quellAngabe.Replace("@URL@", "-");
                        }

                        if (quelle.logEntry != null)
                        {
                            quellAngabe = quellAngabe.Replace("@DOWNLOAD_STAMP@", quelle.logEntry.downloadStamp.Val_Date.Value.ToString());
                        }
                        else
                        {
                            quellAngabe = quellAngabe.Replace("@DOWNLOAD_STAMP@", "-");
                        }

                        if (encodeName)
                        {
                            quelle.quell.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(quellAngabe));
                        }
                        else
                        {
                            quelle.quell.Name = quellAngabe;
                        }

                    }
                }
                
                if (dbReaderCreatedQuelle.ObjAtts.Any())
                {
                    (from quelle in quellen
                     join createdQuelle in dbReaderCreatedQuelle.ObjAtts on quelle.GUID equals createdQuelle.ID_Object
                     select new { quelle, createdQuelle }).ToList().ForEach(quelle =>
                       {
                           if (encodeName)
                           {
                               quelle.quelle.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(quelle.createdQuelle.Val_String));
                           }
                           else
                           {
                               quelle.quelle.Name = quelle.createdQuelle.Val_String;
                           }
                       });
                }
                
                return result;
            });

            return taskResult;
        }

        public InternetQuelleNameController(Globals globals) : base(globals)
        {

        }
    }
}
