﻿using LiteraturQuelleModule.Models;
using LiteraturQuelleModule.Services;
using LogModule;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector.Interfaces;
using LiteraturQuelleModule.Validation;
using LiteraturQuelleModule.Controller;

namespace LiteraturQuelleModule
{
    public class LiteraturQuelleController : AppController
    {
        public string IdClassUrl
        {
            get
            {
                return Config.LocalData.Class_Url.GUID;
            }
        }

        public string IdClassPartner
        {
            get
            {
                return Config.LocalData.Class_Partner.GUID;
            }
        }

        public string IdClassLiteratur
        {
            get
            {
                return Config.LocalData.Class_Literatur.GUID;
            }
        }

        public string IdRelationTypeQuelle
        {
            get
            {
                return Config.LocalData.RelationType_is_subordinated.GUID;
            }
        }

        public string IdDirectionQuelle
        {
            get
            {
                return Globals.Direction_LeftRight.GUID;
            }
        }

        public string IdParentRelationLiterarischeQuelle
        {
            get
            {
                return Config.LocalData.Class_literarische_Quelle.GUID;
            }
        }

        public string IdClassQuellType
        {
            get
            {
                return Config.LocalData.Class_Quell_Types.GUID;
            }
        }

        public async Task<ResultItem<InternetQuelleModel>> GetInternetQuelle(clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<ResultItem<InternetQuelleModel>>(async() =>
           {
               var result = new ResultItem<InternetQuelleModel>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new InternetQuelleModel()
               };

               var elasticAgent = new ElasticAgent(Globals);

               var refItemResult = await elasticAgent.GetOItem(refItem.GUID, refItem.Type);

               result.ResultState = refItemResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = $"Error while getting the refitem: {result.ResultState.Additional1}";
                   return result;
               }

               refItem = refItemResult.Result;

               if (refItem.GUID_Parent == Config.LocalData.Class_Bild_Quelle.GUID ||
                    refItem.GUID_Parent == Config.LocalData.Class_Audio_Quelle.GUID ||
                    refItem.GUID_Parent == Config.LocalData.Class_Video_Quelle.GUID ||
                    refItem.GUID_Parent == Config.LocalData.Class_Zeitungsquelle.GUID ||
                     refItem.GUID_Parent == Config.LocalData.Class_Buch_Quellenangabe.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "The refItem is a source!";
                   return result;
               }

               if (refItem.GUID_Parent == Config.LocalData.Class_literarische_Quelle.GUID)
               {
                   result.Result.LiteraturQuelle = refItem;

               }
               else if (refItem.GUID_Parent == Config.LocalData.Class_Internet_Quellenangabe.GUID)
               {
                   result.Result.InternetQuelle = refItem;
               }
               else
               {
                   var getLiteraturQuellen = await elasticAgent.GetLiteraturQuellenOfRef(refItem);
                   result.ResultState = getLiteraturQuellen.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   if (getLiteraturQuellen.Result.Count == 0)
                   {
                       result.ResultState = Globals.LState_Nothing.Clone();
                       return result;
                   }

                   if (getLiteraturQuellen.Result.Count > 1)
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "More than one Literaturquelle found!";
                       return result;
                   }

                   result.Result.LiteraturQuelle = getLiteraturQuellen.Result.Select(rel => new clsOntologyItem { GUID = rel.ID_Object, Name = rel.Name_Object, GUID_Parent = rel.ID_Parent_Object, Type = Globals.Type_Object }).First();
               }

               if (result.Result.LiteraturQuelle != null && result.Result.InternetQuelle == null)
               {
                   var getInternetQuelle = await elasticAgent.GetSourceOfLiteraturquelle(result.Result.LiteraturQuelle, Config.LocalData.Class_Internet_Quellenangabe.GUID);

                   result.ResultState = getInternetQuelle.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   result.Result.InternetQuelle = getInternetQuelle.Result;


               } else if (result.Result.LiteraturQuelle == null && result.Result.InternetQuelle != null)
               {
                   var checkLiteraturQuelleResult = await elasticAgent.CheckLiteraturQuelleOfSource(result.Result.InternetQuelle);
                   result.ResultState = checkLiteraturQuelleResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   result.Result.LiteraturQuelle = checkLiteraturQuelleResult.Result;
               }

               var getUrlResult = await elasticAgent.GetUrlOfInternetQuelle(result.Result.InternetQuelle);

               result.ResultState = getUrlResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.Url = getUrlResult.Result;


               var getCreatorResult = await elasticAgent.GetPartnerOfInternetQuelle(result.Result.InternetQuelle);

               result.ResultState = getCreatorResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.Autor = getCreatorResult.Result;


               var logController = new LogController(Globals);

               var resultGetLogEntry = await logController.GetLogItems(result.Result.InternetQuelle);

               result.ResultState = resultGetLogEntry.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Download Logentry!";
                   return result;
               }

               result.Result.LogEntry = resultGetLogEntry.Result.FirstOrDefault();

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<BuchQuelleModel>> GetBuchQuelle(clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<ResultItem<BuchQuelleModel>>(async () =>
            {
                var result = new ResultItem<BuchQuelleModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new BuchQuelleModel()
                };

                var elasticAgent = new ElasticAgent(Globals);

                var refItemResult = await elasticAgent.GetOItem(refItem.GUID, refItem.Type);

                result.ResultState = refItemResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = $"Error while getting the refitem: {result.ResultState.Additional1}";
                    return result;
                }

                refItem = refItemResult.Result;

                if (refItem.GUID_Parent == Config.LocalData.Class_Bild_Quelle.GUID ||
                     refItem.GUID_Parent == Config.LocalData.Class_Audio_Quelle.GUID ||
                     refItem.GUID_Parent == Config.LocalData.Class_Video_Quelle.GUID ||
                     refItem.GUID_Parent == Config.LocalData.Class_Zeitungsquelle.GUID ||
                     refItem.GUID_Parent == Config.LocalData.Class_Internet_Quellenangabe.GUID)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The refItem is a source!";
                    return result;
                }

                if (refItem.GUID_Parent == Config.LocalData.Class_literarische_Quelle.GUID)
                {
                    result.Result.LiteraturQuelle = refItem;

                }
                else if (refItem.GUID_Parent == Config.LocalData.Class_Buch_Quellenangabe.GUID)
                {
                    result.Result.BuchQuelle = refItem;
                }
                else
                {
                    var getLiteraturQuellen = await elasticAgent.GetLiteraturQuellenOfRef(refItem);
                    result.ResultState = getLiteraturQuellen.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    if (getLiteraturQuellen.Result.Count == 0)
                    {
                        result.ResultState = Globals.LState_Nothing.Clone();
                        return result;
                    }

                    if (getLiteraturQuellen.Result.Count > 1)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "More than one Literaturquelle found!";
                        return result;
                    }

                    result.Result.LiteraturQuelle = getLiteraturQuellen.Result.Select(rel => new clsOntologyItem { GUID = rel.ID_Object, Name = rel.Name_Object, GUID_Parent = rel.ID_Parent_Object, Type = Globals.Type_Object }).First();
                }

                if (result.Result.LiteraturQuelle != null && result.Result.BuchQuelle == null)
                {
                    var getInternetQuelle = await elasticAgent.GetSourceOfLiteraturquelle(result.Result.LiteraturQuelle, Config.LocalData.Class_Buch_Quellenangabe.GUID);

                    result.ResultState = getInternetQuelle.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.BuchQuelle = getInternetQuelle.Result;


                }
                else if (result.Result.LiteraturQuelle == null && result.Result.BuchQuelle != null)
                {
                    var checkLiteraturQuelleResult = await elasticAgent.CheckLiteraturQuelleOfSource(result.Result.BuchQuelle);
                    result.ResultState = checkLiteraturQuelleResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.LiteraturQuelle = checkLiteraturQuelleResult.Result;
                }

                if (result.Result.BuchQuelle != null)
                {
                    var getSeiteResult = await elasticAgent.GetSeiteOfBuchquelle(result.Result.BuchQuelle);

                    result.ResultState = getSeiteResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.Seite = getSeiteResult.Result;

                    var getLiteratuResult = await elasticAgent.GetLiteraturOfBuchQuelle(result.Result.BuchQuelle);

                    result.ResultState = getSeiteResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.Literatur = getLiteratuResult.Result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<InternetQuelleModel>> SaveNameInternetQuelle(SaveNameInternetQuelleRequest request)
        {
            var taskResult = await Task.Run<ResultItem<InternetQuelleModel>>(async() =>
            {
                var result = new ResultItem<InternetQuelleModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = request
                };

                result.ResultState = await ValidateInternetQuelleRequest(request);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                if (request.Name != request.InternetQuelle.Name)
                {
                    var saveName = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID = request.InternetQuelle.GUID,
                            Name = request.Name,
                            GUID_Parent = request.InternetQuelle.GUID_Parent,
                            Type = request.InternetQuelle.Type
                        }
                    };

                    var dbWriter = new OntologyModDBConnector(Globals);

                    result.ResultState = dbWriter.SaveObjects(saveName);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the name!";
                        return result;
                    }

                    result.Result.InternetQuelle.Name = request.Name;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<BuchQuelleModel>> SaveNameBuchQuelle(SaveNameBuchQuelleRequest request)
        {
            var taskResult = await Task.Run<ResultItem<BuchQuelleModel>>(async () =>
            {
                var result = new ResultItem<BuchQuelleModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = request
                };

                result.ResultState = await ValidateBuchQuelleRequest(request);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                if (request.Name != request.BuchQuelle.Name)
                {
                    var saveName = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID = request.BuchQuelle.GUID,
                            Name = request.Name,
                            GUID_Parent = request.BuchQuelle.GUID_Parent,
                            Type = request.BuchQuelle.Type
                        }
                    };

                    var dbWriter = new OntologyModDBConnector(Globals);

                    result.ResultState = dbWriter.SaveObjects(saveName);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the name!";
                        return result;
                    }

                    result.Result.BuchQuelle.Name = request.Name;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> ValidateInternetQuelleRequest(InternetQuelleModel request)
        {
            var result = Globals.LState_Success.Clone();

            if ((request.RefItem == null && request.LiteraturQuelle == null || request.InternetQuelle == null) || request.UserItem == null || request.GroupItem == null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "Request is not valid!";
                return result;
            }

            var elasticAgent = new ElasticAgent(Globals);

            var objectList = new List<clsOntologyItem>
                {
                    request.RefItem,
                    request.LiteraturQuelle,
                    request.InternetQuelle,
                    request.UserItem,
                    request.GroupItem
                };

            var getOItemsResult = await elasticAgent.GetOItems(objectList, Globals.Type_Object);

            result = getOItemsResult.ResultState;

            if (result.GUID == Globals.LState_Error.GUID)
            {
                result.Additional1 = $"Error while getting objects of request: {result.Additional1}";
                return result;
            }

            request.RefItem = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.RefItem.GUID);

            if (request.RefItem == null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "No Refitem found!";
                return result;
            }

            request.LiteraturQuelle = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.LiteraturQuelle.GUID);

            request.InternetQuelle = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.InternetQuelle.GUID);

            request.UserItem = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.UserItem.GUID);

            if (request.UserItem == null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "No User found!";
                return result;
            }

            request.GroupItem = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.GroupItem.GUID);

            if (request.GroupItem == null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "No Group found!";
                return result;
            }

            return result;
        }

        public async Task<clsOntologyItem> ValidateBuchQuelleRequest(BuchQuelleModel request)
        {
            var result = Globals.LState_Success.Clone();

            if (request.RefItem == null && request.LiteraturQuelle == null || request.BuchQuelle == null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "Request is not valid!";
                return result;
            }

            var elasticAgent = new ElasticAgent(Globals);

            var objectList = new List<clsOntologyItem>
                {
                    request.RefItem,
                    request.LiteraturQuelle,
                    request.BuchQuelle
                };

            var getOItemsResult = await elasticAgent.GetOItems(objectList, Globals.Type_Object);

            result = getOItemsResult.ResultState;

            if (result.GUID == Globals.LState_Error.GUID)
            {
                result.Additional1 = $"Error while getting objects of request: {result.Additional1}";
                return result;
            }

            request.RefItem = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.RefItem.GUID);

            if (request.RefItem == null)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "No Refitem found!";
                return result;
            }

            request.LiteraturQuelle = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.LiteraturQuelle.GUID);

            request.BuchQuelle = getOItemsResult.Result.FirstOrDefault(oItem => oItem.GUID == request.BuchQuelle.GUID);

            return result;
        }

        public async Task<ResultItem<InternetQuelleModel>> SaveDownloadStamp(SaveDownloadStampRequest request)
        {
            var taskResult = await Task.Run<ResultItem<InternetQuelleModel>>(async () =>
            {
                var result = new ResultItem<InternetQuelleModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new InternetQuelleModel()
                };

                result.ResultState = await ValidateInternetQuelleRequest(request);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var logEntryController = new LogController(Globals);

                var logentriesResult = await logEntryController.GetLogItems(request.InternetQuelle);

                result.ResultState = logentriesResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Logentries!";
                    return result;
                }

                if (logentriesResult.Result.Any())
                {
                    var logEntry = logentriesResult.Result.First();

                    var changeResult = await logEntryController.ChangeDateTimeStamp(logEntry, request.downloadStamp);

                    result.ResultState = changeResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    result.Result.LogEntry = changeResult.Result;
                }
                else
                {
                    var logEntryResult = await logEntryController.CreateLogEntry(Config.LocalData.Object_Download, request.InternetQuelle, request.UserItem, "Download", request.downloadStamp, Config.LocalData.RelationType_download, Globals.Direction_LeftRight);

                    result.ResultState = logEntryResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.LogEntry = logEntryResult.Result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<BuchQuelleModel>> SaveSeite(SaveSeiteRequest request)
        {
            var taskResult = await Task.Run<ResultItem<BuchQuelleModel>>(async () =>
            {
                var result = new ResultItem<BuchQuelleModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new BuchQuelleModel()
                };

                result.ResultState = await ValidateBuchQuelleRequest(request);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var relationConfig = new clsRelationConfig(Globals);
                var transaction = new clsTransaction(Globals);

                var relSeite = relationConfig.Rel_ObjectAttribute(request.BuchQuelle, Config.LocalData.AttributeType_Seite, request.Seiten);
                result.ResultState = transaction.do_Transaction(relSeite, boolRemoveAll: true);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the seiten!";
                    return result;
                }

                result.Result.Seite = transaction.OItem_Last.OItemObjectAtt;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultOItem<clsOntologyItem>> GetOItem(string idObject)
        {
            var elasticAgent = new ElasticAgent(Globals);
            var result = await elasticAgent.GetOItem(idObject, Globals.Type_Object);
            return result;
        }

        public async Task<ResultItem<InternetQuelleModel>> SaveInternetReference(SaveInternetRelRequest request)
        {
            var taskResult = await Task.Run<ResultItem<InternetQuelleModel>>(async () =>
            {
                var result = new ResultItem<InternetQuelleModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new InternetQuelleModel()
                };

                result.ResultState = await ValidateInternetQuelleRequest(request);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var elasticAgent = new ElasticAgent(Globals);

                var relItem = await elasticAgent.GetOItem(request.IdRel, Globals.Type_Object);

                result.ResultState = relItem.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Relation-Item!";
                    return result;
                }

                var relationConfig = new clsRelationConfig(Globals);
                var transaction = new clsTransaction(Globals);

                var rel = relationConfig.Rel_ObjectRelation(request.InternetQuelle, relItem.Result, relItem.Result.GUID_Parent == Config.LocalData.Class_Url.GUID ? Config.LocalData.RelationType_belonging_Source : Config.LocalData.RelationType_Ersteller);

                result.ResultState = transaction.do_Transaction(rel, true);


                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the Relation-Item!";
                    return result;
                }

                if (relItem.Result.GUID_Parent == Config.LocalData.Class_Url.GUID)
                {
                    result.Result.Url = relItem.Result;
                }
                else
                {
                    result.Result.Autor = relItem.Result;
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<BuchQuelleModel>> SaveBuchReference(SaveBuchRelRequest request)
        {
            var taskResult = await Task.Run<ResultItem<BuchQuelleModel>>(async () =>
            {
                var result = new ResultItem<BuchQuelleModel>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new BuchQuelleModel()
                };

                result.ResultState = await ValidateBuchQuelleRequest(request);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var elasticAgent = new ElasticAgent(Globals);

                var relItem = await elasticAgent.GetOItem(request.IdRel, Globals.Type_Object);

                result.ResultState = relItem.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Relation-Item!";
                    return result;
                }

                var relationConfig = new clsRelationConfig(Globals);
                var transaction = new clsTransaction(Globals);

                var rel = relationConfig.Rel_ObjectRelation(request.BuchQuelle, relItem.Result, Config.LocalData.RelationType_belonging_Source );

                result.ResultState = transaction.do_Transaction(rel, true);


                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the Relation-Item!";
                    return result;
                }

                result.Result.Literatur = relItem.Result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ResultItem<LiteraturQuellenString>>>> GetLiteraturQuelle(string idLiteraturQuelle = null)
        {
            var taskResult = await Task.Run<ResultItem<List<ResultItem<LiteraturQuellenString>>>>(async() =>
           {
               var result = new ResultItem<List<ResultItem<LiteraturQuellenString>>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<ResultItem<LiteraturQuellenString>>()
               };

               var elasticAgent = new ElasticAgent(Globals);

               var literaturQuellen = new List<clsOntologyItem>();

               var literaturquellenResult = await elasticAgent.GetLiteraturQuellen(idLiteraturQuelle);

               result.ResultState = literaturquellenResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var quelleResult = await elasticAgent.GetSourcesOfLiteraturquellen(literaturquellenResult.Result);

               result.ResultState = quelleResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var audioQuellen = quelleResult.Result.Where(sourceRel => sourceRel.ID_Parent_Object == Config.LocalData.Class_Audio_Quelle.GUID).ToList();
               var bildQuellen = quelleResult.Result.Where(sourceRel => sourceRel.ID_Parent_Object == Config.LocalData.Class_Bild_Quelle.GUID).ToList();
               var buchQuellen = quelleResult.Result.Where(sourceRel => sourceRel.ID_Parent_Object == Config.LocalData.Class_Buch_Quellenangabe.GUID).ToList();
               var emailQuellen = quelleResult.Result.Where(sourceRel => sourceRel.ID_Parent_Object == Config.LocalData.Class_EMail_Quelle.GUID).ToList();
               var internetQuellen = quelleResult.Result.Where(sourceRel => sourceRel.ID_Parent_Object == Config.LocalData.Class_Internet_Quellenangabe.GUID).ToList();
               var videoQuellen = quelleResult.Result.Where(sourceRel => sourceRel.ID_Parent_Object == Config.LocalData.Class_Video_Quelle.GUID).ToList();
               var zeitschriftenQuellen = quelleResult.Result.Where(sourceRel => sourceRel.ID_Parent_Object == Config.LocalData.Class_Zeitungsquelle.GUID).ToList();

               result = await GetLiteraturquelleStringOfBuchQuelle(buchQuellen, elasticAgent);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               var internetQuelleString = await GetLiteraturquelleStringOfInternetQuelle(internetQuellen, elasticAgent);
               result.ResultState = internetQuelleString.ResultState;

               result.Result.AddRange(internetQuelleString.Result);



               return result;
           });

            return taskResult;
        }

        private async Task<ResultItem<List<ResultItem<LiteraturQuellenString>>>> GetLiteraturquelleStringOfInternetQuelle(List<clsObjectRel> internetQuellen, ElasticAgent elasticAgent)
        {
            var result = new ResultItem<List<ResultItem<LiteraturQuellenString>>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ResultItem<LiteraturQuellenString>>()
            };

            var internetQuellenModel = await elasticAgent.GetInternetQuelleModel(internetQuellen.Select(quell => new clsOntologyItem
            {
                GUID = quell.ID_Object,
                Name = quell.Name_Object,
                GUID_Parent = quell.ID_Parent_Object,
                Type = Globals.Type_Object
            }).ToList());


            var downloadStmp = "";
            foreach (var internetQuelle in internetQuellen)
            {
                var internetQuellItem = new ResultItem<LiteraturQuellenString>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new LiteraturQuellenString
                    {
                        LiteraturQuelle = new clsOntologyItem
                        {
                            GUID = internetQuelle.ID_Other,
                            Name = internetQuelle.Name_Other,
                            GUID_Parent = internetQuelle.ID_Parent_Other,
                            Type = internetQuelle.Ontology
                        },
                        Quelle = new clsOntologyItem
                        {
                            GUID = internetQuelle.ID_Object,
                            Name = internetQuelle.Name_Object,
                            GUID_Parent = internetQuelle.ID_Parent_Object,
                            Type = Globals.Type_Object
                        }
                    }
                };

                result.Result.Add(internetQuellItem);

                var downloadStamp = (from logEntry in internetQuellenModel.Result.LogEntries.Where(logE => logE.ID_Object == internetQuelle.ID_Object)
                                     join downStamp in internetQuellenModel.Result.DownloadStamps on logEntry.ID_Other equals downStamp.ID_Object
                                     select downStamp).FirstOrDefault();

                if (downloadStamp != null)
                {
                    downloadStmp = downloadStamp.Val_Date.Value.ToString("dd.MM.yyyy HH:mm");
                }
                else
                {
                    internetQuellItem.ResultState = Globals.LState_Error.Clone();
                    internetQuellItem.ResultState.Additional1 = "No Downloadstamp found!";
                }

                var ersteller = "";
                var erstellerItem = internetQuellenModel.Result.Ersteller.FirstOrDefault(erst => erst.ID_Object == internetQuelle.ID_Object);

                if (erstellerItem != null)
                {
                    ersteller = erstellerItem.Name_Other;
                }
                else
                {
                    internetQuellItem.ResultState = Globals.LState_Error.Clone();
                    internetQuellItem.ResultState.Additional1 = "No Ersteller found!";
                }

                var url = "";
                var urlItem = internetQuellenModel.Result.Urls.FirstOrDefault(erst => erst.ID_Object == internetQuelle.ID_Object);

                if (urlItem != null)
                {
                    url = urlItem.Name_Other;
                }
                else
                {
                    internetQuellItem.ResultState = Globals.LState_Error.Clone();
                    internetQuellItem.ResultState.Additional1 = "No Downloadstamp found!";
                }

                internetQuellItem.Result.QuellenString = $"{ersteller}: {url} (downloaded {downloadStmp}";
            }

            return result;
        }

        private async Task<ResultItem<List<ResultItem<LiteraturQuellenString>>>> GetLiteraturquelleStringOfBuchQuelle(List<clsObjectRel> buchQuellen, ElasticAgent elasticAgent)
        {
            var result = new ResultItem<List<ResultItem<LiteraturQuellenString>>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ResultItem<LiteraturQuellenString>>()
            };
           
            var getBuchquellenModels = await elasticAgent.GetBuchQuellenModel(buchQuellen.Select(quell => new clsOntologyItem
            {
                GUID = quell.ID_Object,
                Name = quell.Name_Object,
                GUID_Parent = quell.ID_Parent_Object,
                Type = Globals.Type_Object
            }).ToList());

            result.ResultState = getBuchquellenModels.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            foreach (var buchQuelle in buchQuellen)
            {
                var quellAutoren = getBuchquellenModels.Result.AutorenOfQuellen.Where(rel => rel.ID_Object == buchQuelle.ID_Object).OrderBy(autor => autor.Name_Other);
                var literaturItem = getBuchquellenModels.Result.Literaturen.FirstOrDefault(rel => rel.ID_Object == buchQuelle.ID_Object);

                var buchQuellItem = new ResultItem<LiteraturQuellenString>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new LiteraturQuellenString
                    {
                        LiteraturQuelle = new clsOntologyItem
                        {
                            GUID = buchQuelle.ID_Other,
                            Name = buchQuelle.Name_Other,
                            GUID_Parent = buchQuelle.ID_Parent_Other,
                            Type = buchQuelle.Ontology
                        },
                        Quelle = new clsOntologyItem
                        {
                            GUID = buchQuelle.ID_Object,
                            Name = buchQuelle.Name_Object,
                            GUID_Parent = buchQuelle.ID_Parent_Object,
                            Type = Globals.Type_Object
                        }
                    }
                };

                result.Result.Add(buchQuellItem);

                if (literaturItem == null)
                {
                    buchQuellItem.ResultState = Globals.LState_Error.Clone();
                    continue;
                }

                var literatur = literaturItem.Name_Other;
                var autoren = "";

                if (quellAutoren.Any())
                {
                    autoren = string.Join(", ", quellAutoren.Select(autor => autor.Name_Other));
                }
                else
                {
                    var literaturAutoren = getBuchquellenModels.Result.AutorenOfLiteraturen.Where(rel => rel.ID_Object == literaturItem.ID_Other);

                    if (literaturAutoren.Any())
                    {
                        autoren = string.Join(", ", literaturAutoren.Select(autor => autor.Name_Other));
                    }
                    else
                    {
                        var literaturHerausgeber = getBuchquellenModels.Result.HerausgeberOfLiteraturen.Where(rel => rel.ID_Object == literaturItem.ID_Other);

                        if (literaturHerausgeber.Any())
                        {
                            autoren = $"Herausgeber {string.Join(", ", literaturHerausgeber.Select(herausgeber => herausgeber.Name_Other))}";
                        }
                        else
                        {
                            var projektLeiter = getBuchquellenModels.Result.ProjektleitungOfLiteraturen.Where(rel => rel.ID_Object == literaturItem.ID_Other);

                            if (projektLeiter.Any())
                            {
                                autoren = $"Projektleiter {string.Join(", ", literaturHerausgeber.Select(herausgeber => herausgeber.Name_Other))}";
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(autoren))
                {
                    buchQuellItem.ResultState = Globals.LState_Error.Clone();
                    buchQuellItem.ResultState.Additional1 = "No Autoren-Information!";   
                }

                var seitenItem = getBuchquellenModels.Result.Seiten.FirstOrDefault(att => att.ID_Object == buchQuelle.ID_Object);
                var seite = "";

                if (seitenItem != null)
                {
                    seite = seitenItem.Val_String;
                }
                else
                {
                    buchQuellItem.ResultState = Globals.LState_Error.Clone();
                    buchQuellItem.ResultState.Additional1 = "No Seiten-Information!";
                }

                var jahr = "";
                var jahrItem = getBuchquellenModels.Result.Jahre.FirstOrDefault(jahrItm => jahrItm.ID_Object == literaturItem.ID_Other);

                if (jahrItem != null)
                {
                    jahr = jahrItem.Val_Name;
                }
                else
                {
                    buchQuellItem.ResultState = Globals.LState_Error.Clone();
                    buchQuellItem.ResultState.Additional1 = "No Jahr-Information!";
                }

                var ort = "";
                var ortItem = getBuchquellenModels.Result.OrteOfLiteraturen.FirstOrDefault(ortItm => ortItm.ID_Object == literaturItem.ID_Other);

                if (ortItem != null)
                {
                    ort = ortItem.Name_Other;
                }
                else
                {
                    buchQuellItem.ResultState = Globals.LState_Error.Clone();
                    buchQuellItem.ResultState.Additional1 = "No Ort-Information!";
                }

                var verlag = "";
                var verlagItem = getBuchquellenModels.Result.VerlageOfLiteraturen.FirstOrDefault(verlagItm => verlagItm.ID_Object == literaturItem.ID_Other);

                if (verlagItem != null)
                {
                    verlag = verlagItem.Name_Other;
                }
                else
                {
                    buchQuellItem.ResultState = Globals.LState_Error.Clone();
                    buchQuellItem.ResultState.Additional1 = "No Verlags-Information!";
                }

                buchQuellItem.Result.QuellenString = $"{autoren}: {literatur} ({verlag} - {ort} {jahr}), S. {seite}";
            }

            return result;
        }

        public async Task<clsOntologyItem> SetQuelleStringOfQuelle(SetQuelleStringOfQuelleRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = Globals.LState_Success.Clone();

                result = ValidationController.ValidateSetQuelleStringOfQuelleRequest(request, Globals);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var elasticAgent = new Services.ElasticAgent(Globals);

                request.MessageOutpupt?.OutputInfo("Get configuration...");
                var configResult = await elasticAgent.GetSetQuelleStringOfQuelleConfig(request);

                result = configResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutpupt?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutpupt?.OutputInfo("Have configuration.");

                var internetQuellController = new InternetQuelleNameController(Globals);

                var setNameResult = await internetQuellController.SetName(configResult.Result);

                return result;
            });

            return taskResult;
        }

        public LiteraturQuelleController(Globals globals) : base(globals)
        {
        }
    }
}
