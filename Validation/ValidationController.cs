﻿using LiteraturQuelleModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateSetQuelleStringOfQuelleRequest(SetQuelleStringOfQuelleRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is not valid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateSetQuelleStringOfQuelleConfig(SetQuelleStringOfQuelleConfig config, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == null || propertyName == nameof(SetQuelleStringOfQuelleConfig.Config))
            {
                if (config.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No config found!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(SetQuelleStringOfQuelleConfig.TemplateInternetQuelle))
            {
                if (config.TemplateInternetQuelle == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Template for Internet-Quellen found! Use Variables (@PARTNER@, @TITLE@, @YEAR@, @URL@, @DOWNLOAD_STAMP@)";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(SetQuelleStringOfQuelleConfig.TemplateBuchQuelle))
            {
                if (config.TemplateInternetQuelle == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Template for Buch-Quellen found! Use Variables (@PARTNER@, @YEAR@, @TITLE@, @AUFLAGE@, @VERLAG@, @ORT@ @SEITEN@)";
                    return result;
                }
            }

            


            return result;
        }
    }
}
