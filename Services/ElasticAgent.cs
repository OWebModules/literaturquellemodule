﻿using LiteraturQuelleModule.Models;
using LiteraturQuelleModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Services
{
    public class ElasticAgent : ElasticBaseAgent
    {
        public async Task<ResultItem<SetQuelleStringOfQuelleConfig>> GetSetQuelleStringOfQuelleConfig(SetQuelleStringOfQuelleRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SetQuelleStringOfQuelleConfig>>(() =>
           {
               var result = new ResultItem<SetQuelleStringOfQuelleConfig>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new SetQuelleStringOfQuelleConfig()
               };

               result.ResultState = ValidationController.ValidateSetQuelleStringOfQuelleRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = SetNameOfQuelle.Config.LocalData.Class_Set_Name_of_Quelle.GUID
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Config!";
                   return result;
               }

               result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateSetQuelleStringOfQuelleConfig(result.Result, globals, nameof(SetQuelleStringOfQuelleConfig.Config));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchTemplateInternetQuelle = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_AttributeType = SetNameOfQuelle.Config.LocalData.AttributeType_Template__Internet_Quelle.GUID
                   }
               };

               var dbReaderTemplateInternetQuelle = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTemplateInternetQuelle.GetDataObjectAtt(searchTemplateInternetQuelle);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the template for Internet-Quellen!";
                   return result;
               }

               result.Result.TemplateInternetQuelle = dbReaderTemplateInternetQuelle.ObjAtts.FirstOrDefault();

               result.ResultState = ValidationController.ValidateSetQuelleStringOfQuelleConfig(result.Result, globals, nameof(SetQuelleStringOfQuelleConfig.TemplateInternetQuelle));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }


               var searchTemplateBuchQuelle = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_AttributeType = SetNameOfQuelle.Config.LocalData.AttributeType_Template__Buchquelle.GUID
                   }
               };

               var dbReaderTemplateBuchQuelle = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTemplateBuchQuelle.GetDataObjectAtt(searchTemplateBuchQuelle);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the template for Buch-Quellen!";
                   return result;
               }

               result.Result.TemplateBuchQuelle = dbReaderTemplateBuchQuelle.ObjAtts.FirstOrDefault();

               result.ResultState = ValidationController.ValidateSetQuelleStringOfQuelleConfig(result.Result, globals, nameof(SetQuelleStringOfQuelleConfig.TemplateBuchQuelle));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchLiteraturQuellenToAnalyze = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = SetNameOfQuelle.Config.LocalData.ClassRel_Set_Name_of_Quelle_contains_literarische_Quelle.ID_RelationType,
                       ID_Parent_Other = SetNameOfQuelle.Config.LocalData.ClassRel_Set_Name_of_Quelle_contains_literarische_Quelle.ID_Class_Right
                   }
               };

               var dbReaderAnalyze = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderAnalyze.GetDataObjectRel(searchLiteraturQuellenToAnalyze);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Quellen to analyze!";
                   return result;
               }

               result.Result.LiteraturQuellenToAnalyze = dbReaderAnalyze.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.LiteraturQuellenToAnalyze.Any())
               {
                   var searchLiteraturQuellen = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID_Parent = Config.LocalData.Class_literarische_Quelle.GUID
                       }
                   };

                   result.ResultState = dbReaderAnalyze.GetDataObjects(searchLiteraturQuellen);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting all Literarische Quellen!";
                       return result;
                   }

                   result.Result.LiteraturQuellenToAnalyze = dbReaderAnalyze.Objects1;
               }

               var searchQuellen = result.Result.LiteraturQuellenToAnalyze.Select(litQuell => new clsObjectRel
               {
                   ID_Other = litQuell.GUID,
                   ID_RelationType = Config.LocalData.RelationType_is_subordinated.GUID
               }).ToList();

               var dbReaderQuellen = new OntologyModDBConnector(globals);

               if (searchQuellen.Any())
               {
                   result.ResultState = dbReaderQuellen.GetDataObjectRel(searchQuellen);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Quellen of Literarische Quellen!";
                       return result;
                   }
               }

               result.Result.QuellenToLiteraturQuellen = dbReaderQuellen.ObjectRels;

               var searchQuellString = result.Result.QuellenToLiteraturQuellen.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Object,
                   ID_AttributeType = Config.LocalData.AttributeType_Quelle.GUID
               }).ToList();

               var dbReaderQuellString = new OntologyModDBConnector(globals);

               if (searchQuellString.Any())
               {
                   result.ResultState = dbReaderQuellString.GetDataObjectAtt(searchQuellString);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Quellstring!";
                       return result;
                   }
               }

               result.Result.QuelleToQuellString = dbReaderQuellString.ObjAtts;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetPartnerOfInternetQuelle(clsOntologyItem internetQuelle)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchPartner = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = internetQuelle.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_Class_Right
                   }
               };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectRel(searchPartner);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Partner of the Internetquelle!";
                    return result;
                }

                result.Result = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetUrlOfInternetQuelle(clsOntologyItem internetQuelle)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
           {
               var result = new ResultItem<clsOntologyItem>
               {
                   ResultState = globals.LState_Success.Clone()
               };

               var searchUrl = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = internetQuelle.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_Class_Right
                   }
               };

               var dbReader = new OntologyModDBConnector(globals);

               result.ResultState = dbReader.GetDataObjectRel(searchUrl);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Url of the Internetquelle!";
                   return result;
               }

               result.Result = dbReader.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetLiteraturOfBuchQuelle(clsOntologyItem buchQuelle)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchUrl = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = buchQuelle.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Buch_Quellenangabe_belonging_Source_Literatur.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Buch_Quellenangabe_belonging_Source_Literatur.ID_Class_Right
                   }
               };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectRel(searchUrl);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Literatur of the Buchquellenangabe!";
                    return result;
                }

                result.Result = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetLiteraturQuellenOfRef(clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                if (refItem.GUID_Parent == Config.LocalData.Class_literarische_Quelle.GUID ||
                   refItem.GUID_Parent == Config.LocalData.Class_Internet_Quellenangabe.GUID ||
                   refItem.GUID_Parent == Config.LocalData.Class_Bild_Quelle.GUID ||
                   refItem.GUID_Parent == Config.LocalData.Class_Audio_Quelle.GUID ||
                   refItem.GUID_Parent == Config.LocalData.Class_Video_Quelle.GUID ||
                   refItem.GUID_Parent == Config.LocalData.Class_Zeitungsquelle.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The refItem is a source or a Literaturquelle";
                    return result;
                }

                var dbReader = new OntologyModDBConnector(globals);

                var searchLiteraturQuellen = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = refItem.GUID,
                        ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                        ID_Parent_Object = Config.LocalData.Class_literarische_Quelle.GUID
                    }
                };

                result.ResultState = dbReader.GetDataObjectRel(searchLiteraturQuellen);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Literaturquellen of refItem";
                    return result;
                }

                result.Result = dbReader.ObjectRels;

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetSourcesOfLiteraturquellen(List<clsOntologyItem> literaturQuellen)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
           {
               var result = new ResultItem<List<clsObjectRel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsObjectRel>()
               };

               if (!literaturQuellen.Any())
               {
                   result.ResultState = globals.LState_Nothing.Clone();
                   return result;
               }

               var searchQuelle = literaturQuellen.Select(obj => new clsObjectRel
               {
                   ID_Other = obj.GUID,
                   ID_RelationType = Config.LocalData.RelationType_is_subordinated.GUID
               }).ToList();


               var dbReaderQuelle = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderQuelle.GetDataObjectRel(searchQuelle);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Quellen!";
                   return result;
               }

               result.Result = dbReaderQuelle.ObjectRels;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<InternetQuellenModel>> GetInternetQuelleModel(List<clsOntologyItem> quellen)
        {
            var taskResult = await Task.Run<ResultItem<InternetQuellenModel>>(() =>
            {
                var result = new ResultItem<InternetQuellenModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new InternetQuellenModel()
                };

                if (!quellen.Any())
                {
                    return result;
                }

                result.Result.InternetQuellen = quellen;

                var searchLogEntries = quellen.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_download_Logentry.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_download_Logentry.ID_Class_Right
                }).ToList();

                var dbReaderLogentries = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderLogentries.GetDataObjectRel(searchLogEntries);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the logentries!";
                    return result;
                }

                result.Result.LogEntries = dbReaderLogentries.ObjectRels;

                var searchDownloadStamps = result.Result.LogEntries.Select(obj => new clsObjectAtt
                {
                    ID_Object = obj.ID_Other,
                    ID_AttributeType = Config.LocalData.AttributeType_DateTimestamp.GUID
                }).ToList();

                var dbReaderDownloadStamps = new OntologyModDBConnector(globals);

                if (searchDownloadStamps.Any())
                {
                    result.ResultState = dbReaderDownloadStamps.GetDataObjectAtt(searchDownloadStamps);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Downloadstamp!";
                        return result;
                    }
                }

                result.Result.DownloadStamps = dbReaderDownloadStamps.ObjAtts;

                var searchErsteller = quellen.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_Ersteller_Partner.ID_Class_Right
                }).ToList();

                var dbReaderErsteller = new OntologyModDBConnector(globals);

                if (searchErsteller.Any())
                {
                    result.ResultState = dbReaderErsteller.GetDataObjectRel(searchErsteller);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Ersteller!";
                        return result;
                    }

                    result.Result.Ersteller = dbReaderErsteller.ObjectRels;
                }

                var searchUrl = quellen.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Internet_Quellenangabe_belonging_Source_Url.ID_Class_Right
                }).ToList();

                var dbReaderUrl = new OntologyModDBConnector(globals);

                if (searchUrl.Any())
                {
                    result.ResultState = dbReaderUrl.GetDataObjectRel(searchUrl);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Url!";
                        return result;
                    }
                }

                result.Result.Urls = dbReaderUrl.ObjectRels;

                var searchPDFDocument = quellen.Select(obj => new clsObjectRel
                {
                    ID_Other = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_PDF_Documents_belongs_to.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_PDF_Documents_belongs_to.ID_Class_Left
                }).ToList();

                var dbReaderPDFDocument = new OntologyModDBConnector(globals);

                if (searchPDFDocument.Any())
                {
                    result.ResultState = dbReaderPDFDocument.GetDataObjectRel(searchPDFDocument);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the PDF-Documents!";
                        return result;
                    }
                }

                result.Result.PDFDocuments = dbReaderPDFDocument.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<BuchQuellenModel>> GetBuchQuellenModel(List<clsOntologyItem> quellen)
        {
            var taskResult = await Task.Run<ResultItem<BuchQuellenModel>>(() =>
           {
               var result = new ResultItem<BuchQuellenModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new BuchQuellenModel()
               };

               if (!quellen.Any())
               {
                   return result;
               }

               var searchSeiten = quellen.Select(quelle => new clsObjectAtt
               {
                   ID_Object = quelle.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Seite.GUID
               }).ToList();

               var dbReaderSeiten = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSeiten.GetDataObjectAtt(searchSeiten);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Seiten!";
                   return result;
               }

               result.Result.Seiten = dbReaderSeiten.ObjAtts;

               var searchLiteraturen = quellen.Select(quelle => new clsObjectRel
               {
                   ID_Object = quelle.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Buch_Quellenangabe_belonging_Source_Literatur.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Buch_Quellenangabe_belonging_Source_Literatur.ID_Class_Right
               }).ToList();

               var dbReaderLiteraturen = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderLiteraturen.GetDataObjectRel(searchLiteraturen);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Literaturen!";
                   return result;
               }

               result.Result.Literaturen = dbReaderLiteraturen.ObjectRels;

               var searchQuellAutoren = quellen.Select(quelle => new clsObjectRel
               {
                   ID_Object = quelle.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Buch_Quellenangabe_Autor_Partner.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Buch_Quellenangabe_Autor_Partner.ID_Class_Right
               }).ToList();

               var dbReaderQuellAutoren = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderQuellAutoren.GetDataObjectRel(searchQuellAutoren);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Quell-Autoren!";
                   return result;
               }

               result.Result.AutorenOfQuellen = dbReaderQuellAutoren.ObjectRels;

               var searchLiteraturAttribute = result.Result.Literaturen.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Other,
                   ID_AttributeType = Config.LocalData.AttributeType_Jahr.GUID
               }).ToList();

               searchLiteraturAttribute.AddRange(result.Result.Literaturen.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Other,
                   ID_AttributeType = Config.LocalData.AttributeType_Auflage.GUID
               }));

               var dbReaderAttribute = new OntologyModDBConnector(globals);

               if (searchLiteraturAttribute.Any())
               {
                   result.ResultState = dbReaderAttribute.GetDataObjectAtt(searchLiteraturAttribute);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the attributes of Literaturen!";
                       return result;
                   }

                   result.Result.Jahre = dbReaderAttribute.ObjAtts.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Jahr.GUID).ToList();
                   result.Result.Auflagen = dbReaderAttribute.ObjAtts.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Auflage.GUID).ToList();
               }

               var searchLiteraturRelations = result.Result.Literaturen.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Literatur_Autor_Partner.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Literatur_Autor_Partner.ID_Class_Right
               }).ToList();

               searchLiteraturRelations.AddRange(result.Result.Literaturen.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Literatur_Herausgeber_Partner.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Literatur_Herausgeber_Partner.ID_Class_Right
               }));

               searchLiteraturRelations.AddRange(result.Result.Literaturen.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Literatur_Projektleitung_Partner.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Literatur_Projektleitung_Partner.ID_Class_Right
               }));

               searchLiteraturRelations.AddRange(result.Result.Literaturen.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Literatur_Verlag_Partner.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Literatur_Verlag_Partner.ID_Class_Right
               }));

               searchLiteraturRelations.AddRange(result.Result.Literaturen.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Literatur_entstand_in_Ort.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Literatur_entstand_in_Ort.ID_Class_Right
               }));

               var dbReaderLiteraturRelations = new OntologyModDBConnector(globals);

               if (searchLiteraturRelations.Any())
               {
                   result.ResultState = dbReaderLiteraturRelations.GetDataObjectRel(searchLiteraturRelations);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Literatu-Relations!";
                       return result;
                   }
               }

               result.Result.HerausgeberOfLiteraturen = dbReaderLiteraturRelations.ObjectRels.Where(rel => rel.ID_RelationType == Config.LocalData.RelationType_Herausgeber.GUID).ToList();
               result.Result.AutorenOfLiteraturen = dbReaderLiteraturRelations.ObjectRels.Where(rel => rel.ID_RelationType == Config.LocalData.RelationType_Autor.GUID).ToList();
               result.Result.ProjektleitungOfLiteraturen = dbReaderLiteraturRelations.ObjectRels.Where(rel => rel.ID_RelationType == Config.LocalData.RelationType_Projektleitung.GUID).ToList();
               result.Result.VerlageOfLiteraturen = dbReaderLiteraturRelations.ObjectRels.Where(rel => rel.ID_RelationType == Config.LocalData.RelationType_Verlag.GUID).ToList();
               result.Result.OrteOfLiteraturen = dbReaderLiteraturRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Ort.GUID).ToList();

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetSourceOfLiteraturquelle(clsOntologyItem literaturQuelle, string idClassSource)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchSource = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = literaturQuelle.GUID,
                        ID_RelationType = Config.LocalData.RelationType_is_subordinated.GUID,
                        ID_Parent_Object = idClassSource
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectRel(searchSource);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the related source!";
                    return result;
                }

                if (dbReader.ObjectRels.Count != 1)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"The number {dbReader.ObjectRels.Count} is incorrect. It must be one Source!";
                    return result;
                }

                result.Result = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).First();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsObjectAtt>> GetSeiteOfBuchquelle(clsOntologyItem buchQuelle)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectAtt>>(() =>
            {
                var result = new ResultItem<clsObjectAtt>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchSeite = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = buchQuelle.GUID,
                        ID_AttributeType = Config.LocalData.AttributeType_Seite.GUID
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectAtt(searchSeite);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the seite Attribute!";
                    return result;
                }

                result.Result = dbReader.ObjAtts.FirstOrDefault();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> CheckLiteraturQuelleOfSource(clsOntologyItem source)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
           {
               var result = new ResultItem<clsOntologyItem>
               {
                   ResultState = globals.LState_Success.Clone()
               };

               var searchLiteraturQuelle = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = source.GUID,
                       ID_RelationType = Config.LocalData.RelationType_is_subordinated.GUID,
                       ID_Parent_Other = Config.LocalData.Class_literarische_Quelle.GUID
                   }
               };

               var dbConnector = new OntologyModDBConnector(globals);

               result.ResultState = dbConnector.GetDataObjectRel(searchLiteraturQuelle);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Literaturquelle of source!";
                   return result;
               }

               result.Result = dbConnector.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).FirstOrDefault();


               if (result.Result == null)
               {
                   var literaturQuelle = new clsOntologyItem
                   {
                       GUID = globals.NewGUID,
                       Name = source.Name,
                       GUID_Parent = Config.LocalData.Class_literarische_Quelle.GUID,
                       Type = globals.Type_Object
                   };

                   result.ResultState = dbConnector.SaveObjects(new List<clsOntologyItem> { literaturQuelle });

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the Literaturquelle!";
                       return result;
                   }

                   var relationConfig = new clsRelationConfig(globals);
                   var relSourceToLiteraturquelle = relationConfig.Rel_ObjectRelation(source, literaturQuelle, Config.LocalData.RelationType_is_subordinated);

                   result.ResultState = dbConnector.SaveObjRel(new List<clsObjectRel>
                   {
                       relSourceToLiteraturquelle
                   });

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       dbConnector.DelObjects(new List<clsOntologyItem>
                       {
                           literaturQuelle
                       });

                       result.ResultState.Additional1 = "Error while saving the relation between source and Literaturquelle!";
                       return result;
                   }

                   result.Result = literaturQuelle;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetOItems(List<clsOntologyItem> oItems, string type)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchItems = oItems.Select(oItem => new clsOntologyItem
                {
                    GUID = oItem.GUID,
                    Type = type
                }).ToList();

                var dbReader = new OntologyModDBConnector(globals);

                if (type == globals.Type_Class)
                {
                    result.ResultState = dbReader.GetDataClasses(searchItems);
                    result.Result = dbReader.Classes1;
                    if (result.Result == null)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Classes found!";
                        return result;
                    }
                }
                else if (type == globals.Type_AttributeType)
                {
                    result.ResultState = dbReader.GetDataAttributeType(searchItems);
                    result.Result = dbReader.AttributeTypes;
                    if (result.Result == null)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Attributetypes found!";
                        return result;
                    }
                }
                else if (type == globals.Type_RelationType)
                {
                    result.ResultState = dbReader.GetDataRelationTypes(searchItems);
                    result.Result = dbReader.RelationTypes;
                    if (result.Result == null)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Relationtypes found!";
                        return result;
                    }
                }
                else if (type == globals.Type_Object)
                {
                    result.ResultState = dbReader.GetDataObjects(searchItems);
                    result.Result = dbReader.Objects1;
                    if (result.Result == null)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Objects found!";
                        return result;
                    }
                }
                else
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Wrong type!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetLiteraturQuellen(string idLiteraturQuelle = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<clsOntologyItem>()
               };

               var searchLiteraturQuellen = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = idLiteraturQuelle,
                       GUID_Parent = Config.LocalData.Class_literarische_Quelle.GUID
                   }
               };

               var dbReaderLiteraturquellen = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderLiteraturquellen.GetDataObjects(searchLiteraturQuellen);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Literaturquellen!";
                   return result;
               }

               result.Result = dbReaderLiteraturquellen.Objects1;

               return result;
           });

            return taskResult;

        }

        public ElasticAgent(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
