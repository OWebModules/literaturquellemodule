﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class SaveSeiteRequest : BuchQuelleModel
    {
        public string Seiten { get; set; }
    }
}
