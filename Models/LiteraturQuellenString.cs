﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class LiteraturQuellenString
    {
        public clsOntologyItem LiteraturQuelle { get; set; }
        public clsOntologyItem Quelle { get; set; }

        public string QuellenString { get; set; }
    }
}
