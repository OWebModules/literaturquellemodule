﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class CompleteLiteraturQuelleNameByOntologyResult
    {
        public List<clsOntologyItem> SuccessfulCompletions { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> UnsuccessfulCompletions { get; set; } = new List<clsOntologyItem>();
    }
}
