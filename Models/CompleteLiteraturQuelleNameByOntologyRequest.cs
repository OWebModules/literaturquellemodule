﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class CompleteLiteraturQuelleNameByRelationsRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }
        public CompleteLiteraturQuelleNameByRelationsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
