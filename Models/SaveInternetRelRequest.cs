﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class SaveInternetRelRequest: InternetQuelleModel
    {
        public string IdRel { get; set; }
    }
}
