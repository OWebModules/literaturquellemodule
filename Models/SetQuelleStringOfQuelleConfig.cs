﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class SetQuelleStringOfQuelleConfig
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectAtt TemplateInternetQuelle { get; set; }
        public clsObjectAtt TemplateBuchQuelle { get; set; }
        public List<clsOntologyItem> LiteraturQuellenToAnalyze { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> QuellenToLiteraturQuellen { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> QuelleToQuellString { get; set; } = new List<clsObjectAtt>();

    }
}
