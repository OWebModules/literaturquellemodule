﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class SaveNameBuchQuelleRequest : BuchQuelleModel
    {
        public string Name { get; set; }
    }
}
