﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class InternetQuellenModel
    {
        public List<clsOntologyItem> InternetQuellen { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> Ersteller { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> LogEntries { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> DownloadStamps { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> Urls { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> PDFDocuments { get; set; } = new List<clsObjectRel>();
    }
}
