﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class BuchQuellenModel
    {
        public List<clsObjectRel> Quellen { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> AutorenOfQuellen { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> Seiten { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> Literaturen { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> Jahre { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectAtt> Auflagen { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> VerlageOfLiteraturen { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> OrteOfLiteraturen { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> AutorenOfLiteraturen { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> HerausgeberOfLiteraturen { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ProjektleitungOfLiteraturen { get; set; } = new List<clsObjectRel>();
    }
}
