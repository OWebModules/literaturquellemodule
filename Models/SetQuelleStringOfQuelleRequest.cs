﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class SetQuelleStringOfQuelleRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutpupt { get; set; }

        public SetQuelleStringOfQuelleRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
