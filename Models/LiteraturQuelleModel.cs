﻿using LogModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteraturQuelleModule.Models
{
    public class LiteraturQuelleModel
    {
        public clsOntologyItem LiteraturQuelle { get; set; }
        public clsOntologyItem Quelle { get; set; }

    }

    public class BuchQuelleModel
    {
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem BuchQuelle { get; set; }

        public clsObjectAtt Seite { get; set; }

        public clsOntologyItem Literatur { get; set; }

        public clsOntologyItem LiteraturQuelle { get; set; }
    }

    public class InternetQuelleModel
    {
        public clsOntologyItem RefItem { get; set; }
        public LogItem LogEntry { get; set; }
        public clsOntologyItem InternetQuelle { get; set; }
        public clsOntologyItem Autor { get; set; }
        public clsOntologyItem Url { get; set; }
        public clsOntologyItem LiteraturQuelle { get; set; }
        public clsOntologyItem UserItem { get; set; }
        public clsOntologyItem GroupItem { get; set; }
    }

    public class AudioQuelleModel
    {
        public InternetQuelleModel InternetQuelle { get; set; }
        public clsOntologyItem Ausstrahlung { get; set; }
        public clsObjectAtt DatetimeStamp { get; set; }
        public clsOntologyItem RadioSender { get; set; }
        public clsOntologyItem LiteraturQuelle { get; set; }
    }

    public class BildQuelleModel
    {
        public clsOntologyItem Ersteller { get; set; }
        public clsOntologyItem LiteraturQuelle { get; set; }

    }
}
